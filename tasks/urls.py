from django.contrib.auth import views as auth_views
from django.shortcuts import redirect
from django.urls import path
from tasks.views import TaskList,  TaskCreate, TaskUpdateView
# from users.views import signup



urlpatterns = [
    path("list/", TaskList.as_view(), name="list"),
    path("create/", TaskCreate.as_view(),name="create"),
    path("<int:pk>/complete/", TaskUpdateView.as_view(), name="complete_task"),
    path("list/>page=1", TaskList.as_view(), name="listpage1")
]
