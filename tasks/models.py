from django.db import models
from django.forms import CharField
from datetime import datetime
from django.conf import settings
from datetime import timedelta

class Difficulty(models.Model):
    importance = models.CharField(max_length=100)
    expgiven = models.IntegerField()

    def __str__(self):
        return str(self.importance)

class Task(models.Model):
    def get_deadline():
        return datetime.now() + timedelta(hours=1)

    name = models.CharField(max_length=100)
    start_time = models.TimeField(auto_now_add=True, blank=True)
    due_date = models.DateTimeField(default=get_deadline)
    is_completed = models.BooleanField(null=True)
    assignee = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="tasks",
        on_delete=models.SET_NULL,
        null=True,
    )
    difficulty = models.ForeignKey(Difficulty, related_name="tasks", on_delete=models.PROTECT)
    established = models.BooleanField(default=False)

    

# Create your models here.
