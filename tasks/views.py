from django.shortcuts import render
from users.models import User
from tasks.models import Difficulty, Task
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy, reverse
# Create your views here.

class TaskList(ListView):
    model = Task
    template_name = "tasks/list.html"
    paginate_by = 10

    def get_queryset(self):
        return Task.objects.filter(assignee=self.request.user)
    

class TaskCreate(CreateView):
    model = Task
    template_name = "tasks/create.html"
    fields = ["name", "difficulty"]
    success_url = reverse_lazy("home")
    def form_valid(self, form):
        form.instance.assignee = self.request.user
        
        return super().form_valid(form)

class TaskUpdateView(UpdateView):
    model = Task
    fields = ["is_completed"]
    success_url = reverse_lazy("home")
    
    def form_valid(self, form):
        print(form.instance.difficulty)
        if str(form.instance.difficulty) in "Very important":
            print("succes")
            self.request.user.exp += 30
            self.request.user.save()
        if str(form.instance.difficulty) in "Average":
            print("succes2")
            self.request.user.exp += 15
            self.request.user.save()
        
        return super().form_valid(form)

class BaseListView(ListView):
    model = User
    template_name = "templates/base.html"

    def get_queryset(self):
        return User.objects.filter(user=self.request.user)

