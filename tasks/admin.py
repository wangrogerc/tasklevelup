from django.contrib import admin
from tasks.models import Task, Difficulty

class TaskAdmin(admin.ModelAdmin):
    pass

class DifficultyAdmin(admin.ModelAdmin):
    pass

admin.site.register(Task, TaskAdmin)
admin.site.register(Difficulty, DifficultyAdmin)
# Register your models here.
